## [1.0.1] - 2018-09-13

### Changed
 - Smaller chronometer widget to avoid involuntary taps

### Fixed
 - Do networking on its own thread

## [1.0.0] - 2018-08-04

### Added
 - Server script that produces PgUp/PgDown events
 - Android app that relays VolUp/VolDown keypresses
 - Chronometer in client app
