# Slide Changer

Slide Changer is a no-nonsense app to change presentation slides on a computer from a phone.

## Installation

Install the client app on an Android phone. This is only tested with
Oreo, but I believe Jelly Bean should work. See [tags](tags) for a
precompiled package.

To install the server app, simply clone the repository into a computer running GNU/Linux. Make sure that `netcat`, `xdotool`, and `avahi` are installed.

## Usage

The computer and phone must be on the same network, either via WiFi or
Bluetooth. It is recommended to disconnect the computer from any other
network before launching the server to avoid unauthorized parties
sending PgUp/PgDown keystrokes.

On the computer, launch the server
```
$ ./server/server.sh
```
and advertise its location
```
$ ./server/advertiser.sh
```
then launch the slide deck.

Start the Slide Changer app on the phone. Press Volume Down to advance
one slide and Volume Up to go back one slide. Tap on the timer to
start/stop, and long tap to reset.
