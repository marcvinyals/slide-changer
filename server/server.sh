#!/bin/bash

nc -l -p 4444 | while read cmd; do
    case $cmd in
        VolumeUp)
            xdotool key Page_Up
            ;;
        VolumeDown)
            xdotool key Page_Down
            ;;
        *)
            echo $cmd
            ;;
    esac
done    
