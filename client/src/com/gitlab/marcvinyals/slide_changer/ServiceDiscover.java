package com.gitlab.marcvinyals.slide_changer;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.content.Context;

public class ServiceDiscover {

  public static final String SERVICE_TYPE = "_slidechanger._tcp";

  NsdManager nsd;

  IParamCallback<String> statusMessage;
  IParamCallback<NsdServiceInfo> successCallback;

  void statusMessage(String message) {
    statusMessage.run(message);
  }
  
  public ServiceDiscover(Context context) {
    nsd = (NsdManager)context.getSystemService(Context.NSD_SERVICE);
  }

  public void discover(IParamCallback<String> statusMessage, IParamCallback<NsdServiceInfo> successCallback) {
    this.statusMessage = statusMessage;
    this.successCallback = successCallback;
    nsd.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, new DiscoveryListener());
  }
  
  class DiscoveryListener implements NsdManager.DiscoveryListener {
  @Override
  public void onStopDiscoveryFailed(String serviceType, int errorCode) {
    statusMessage("Stop Discovery failed");
  }
 
  @Override
  public void onStartDiscoveryFailed(String serviceType, int errorCode) {
    statusMessage("Start Discovery failed");
  }

  @Override
  public void onServiceLost(NsdServiceInfo service) {
    statusMessage("Service Lost");
  }

  @Override
  public void onServiceFound(NsdServiceInfo service) {
    statusMessage("Service Found");
    nsd.resolveService(service, new ResolveListener());
    nsd.stopServiceDiscovery(this);
  }

  @Override
  public void onDiscoveryStopped(String serviceType) {
    statusMessage("Discovery Stopped");
  }
 
  @Override
  public void onDiscoveryStarted(String serviceType) {
    statusMessage("Discovery Started");
  }

  }

  class ResolveListener implements NsdManager.ResolveListener {
  @Override
  public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
    statusMessage("Resolve Failed");
  }
  
  @Override
  public void onServiceResolved(NsdServiceInfo serviceInfo) {
    statusMessage("Service Resolved");
    successCallback.run(serviceInfo);
  }
  }
        
}
