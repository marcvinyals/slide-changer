package com.gitlab.marcvinyals.slide_changer;

import android.app.Activity;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SlideChanger extends Activity
{

  TcpClient tcp;
  ServiceDiscover discover;

  RelativeLayout layout;
  LinearLayout timerLayout;
  AutoChronometer timer;
  TextView debugInfo;
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    layout = new RelativeLayout(this);
    layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

    timerLayout = new LinearLayout(this);
    RelativeLayout.LayoutParams timerLayoutPos = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    timerLayoutPos.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
    timerLayout.setLayoutParams(timerLayoutPos);

    timer = new AutoChronometer(this);
    LinearLayout.LayoutParams timerPos = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
    timer.setLayoutParams(timerPos);

    debugInfo = new TextView(this);
    debugInfo.setText("Initializing...");
    RelativeLayout.LayoutParams debugPos = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    debugPos.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
    debugInfo.setLayoutParams(debugPos);

    layout.addView(debugInfo);
    timerLayout.addView(timer);
    timerLayout.setGravity(Gravity.CENTER);
    //timerLayout.setWeightSum(1.0f);
    layout.addView(timerLayout);
    setContentView(layout);

    timer.setTextSize(72);
    
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }
  
  @Override
  public void onStart() {
    super.onStart();
    discover = new ServiceDiscover(this);
    discover.discover(new StatusMessage(), new DiscoverTask());
  }

  class StatusMessage implements IParamCallback<String> {
    @Override
    public void run(final String message) {
      runOnUiThread(new Runnable() {
          @Override
          public void run() {
            debugInfo.setText(message);
          }
        }
        );
    }
  }

  class DiscoverTask implements IParamCallback<NsdServiceInfo> {
    @Override
    public void run(final NsdServiceInfo serviceInfo) {
      runOnUiThread(new Runnable() {
          @Override
          public void run() {
            tcp = new TcpClient(serviceInfo.getHost(), serviceInfo.getPort());
            new ConnectTask().execute();
          }
        }
        );
    }
  }
  
  class ConnectTask extends AsyncTask<String,Void,String> {
    @Override
    protected String doInBackground(String... params) {
      return tcp.connect();
    }
    protected void onPostExecute(String result) {
      debugInfo.setText(result);
    }
  }

  class SendTask extends AsyncTask<String,Void,Void> {
    @Override
    protected Void doInBackground(String... params) {
      tcp.sendMessage(params[0]);
      return null;
    }
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent event) {
    if (tcp == null) {
      debugInfo.setText("Keypress ignored");
      return super.dispatchKeyEvent(event);
    }
    int keyCode = event.getKeyCode();
    int action = event.getAction();
    switch (keyCode) {
    case KeyEvent.KEYCODE_VOLUME_UP:
      if (action == KeyEvent.ACTION_DOWN) {
        debugInfo.setText("Volume Up pressed");
        new SendTask().execute("VolumeUp");
      }
      return true;
    case KeyEvent.KEYCODE_VOLUME_DOWN:
      if (action == KeyEvent.ACTION_DOWN) {
        debugInfo.setText("Volume Down pressed");
        new SendTask().execute("VolumeDown");
      }
      return true;
    default:
      return super.dispatchKeyEvent(event);
    }
  }
}
