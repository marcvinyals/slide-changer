package com.gitlab.marcvinyals.slide_changer;

import android.content.Context;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Chronometer;

public class AutoChronometer extends Chronometer {

  boolean isRunning = false;
  long elapsed = 0;

  public AutoChronometer(Context context) {
    super(context);
    setOnClickListener(onClickListener);
    setOnLongClickListener(onLongClickListener);
  }

  private OnClickListener onClickListener = new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (isRunning) {
          stop();
          elapsed=SystemClock.elapsedRealtime()-getBase();
        }
        else {
          setBase(SystemClock.elapsedRealtime()-elapsed);
          start();
        }
        isRunning = !isRunning;
      }
    };

  private OnLongClickListener onLongClickListener = new OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        stop();
        isRunning=false;
        elapsed=0;
        setBase(SystemClock.elapsedRealtime());
        return true;
      }
    };
}
