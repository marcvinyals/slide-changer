package com.gitlab.marcvinyals.slide_changer;

import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.IOError;
import java.net.InetAddress;
import java.net.Socket;
 
public class TcpClient {

  //public static final String SERVER_IP = "192.168.31.185";
  //InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
  //public static final int SERVER_PORT = 4444;

  InetAddress serverAddr;
  int serverPort;

  Socket socket;
  PrintWriter buffer;

  public TcpClient(InetAddress serverAddr, int serverPort) {
    this.serverAddr = serverAddr;
    this.serverPort = serverPort;
  }
 
  public void sendMessage(String message) {
    if (!buffer.checkError()) {
      buffer.println(message);
      buffer.flush();
    }
  }
  
  public String connect() {
    try {
      socket = new Socket(serverAddr, serverPort);
      buffer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
    }
    catch (Exception e) {
      return e.toString();
    }
    return ("Connected");
  }
}
