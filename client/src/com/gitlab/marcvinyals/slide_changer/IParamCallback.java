package com.gitlab.marcvinyals.slide_changer;

public interface IParamCallback<T> {
  public void run(T param);
}
